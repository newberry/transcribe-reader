import { PrismaClient, Prisma } from '@prisma/client'
import itemids from '$lib/data/itemids.json'


const oldmeka = new PrismaClient({ datasources: { db: { url: "mysql://username:password@localhost:3306/transcribe_omeka" }} })
const newmeka = new PrismaClient({ datasources: { db: { url: "mysql://username:password@localhost:3306/omekanew" }} }) //lowest = 1411

        // test new item : http://localhost:5174/1578
        
export async function getItemPages(itemid){
    const id = parseInt(itemid.item)
    const prisma = parseInt(id) < 1411 ? oldmeka : newmeka
    const itemPages = await prisma.omeka_files.findMany({
        where: { item_id: id }
    })
    // console.log("itemPages", itemPages)
    const pageIds = await itemPages.map(ip => ip.id)
    let titleElement = await prisma.omeka_element_texts.findFirst({
        where: {AND: [
            {record_id: id},
            {element_id: 50}
        ]}
    })
    // console.log("titleElement.text",titleElement.text)
    const title = titleElement?.text
    let imageElement = await prisma.omeka_element_texts.findFirst({
        where: {AND: [
            {record_id: id},
            {element_id: 48}
        ]}
    })
    let image = imageElement?.text
    if (parseInt(id) > 1410){

    }
    const transcriptions = await prisma.omeka_element_texts.findMany({
        where: {AND: [
            { record_id: { in: pageIds }},
            {element_id: 52}
        ]
    }})
    let pageImageGetter = await prisma.omeka_files.findMany({
        where: {id: {in: pageIds}}
    })
    let pageImages = {}
    for (var i in pageImageGetter){
        pageImages[pageImageGetter[i].id] = pageImageGetter[i].filename
    }
    return [itemPages, transcriptions, title, image, pageImages]
}

// export async function getDescriptions(itemid){
//     const itemDesc = await prisma.omeka_element_texts.findMany({
//         where:{AND: [ { record_id: {in: itemids} },
//             {element_id: 41}
//         ]}
//     })
//     return itemDesc

// } 


export async function getItems(){

    const itemData = await newmeka.omeka_items.findMany({
        where: {
            public: 1
        }
    })
    const itemIdList = await itemData.map(i => i.id)
    const elementData = await newmeka.omeka_element_texts.findMany({
        where: {
            AND: [
                {
                    record_id: {
                        in: itemIdList
                    }
                },
                {
                    OR: [

                        {element_id: 48},
                        {element_id: 50}
                    ]
                }
            ]
        }
    })
    return [itemData, elementData]
}


async function getItemTitle(id){
    let titleElement = await newmeka.omeka_element_texts.findFirst({
        where: {
            AND: [
                {record_id: id},
                {element_id: 50}
            ]
        }
    })
    const itemTitle = titleElement.text
    return itemTitle
}

async function getItemImage(id){
    let imageElement = await newmeka.omeka_element_texts.findFirst({
        where: {
            AND: [
                {record_id: id},
                {element_id: 48}
            ]
        }
    })
    let image = await imageElement.text
    return image

}
// async function getFileTranscription(pageid){
//     const fileTranscription = await prisma.omeka_element_texts.findFirst({
//         where: {
//             AND: [
//                 {element_id: 52},
//                 {record_id: pageid}
//             ]
//         }
//     })
//     return fileTranscription
// }