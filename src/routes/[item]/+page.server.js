import { getItemPages } from "$lib/db/qs";

export function load({params}) {
    const itemData = getItemPages(params)
    return {data: itemData};
}