import { writable } from "svelte/store";

const today = new Date
export const pageTitle = writable(`User-Contributed Transcriptions as of ${today.toLocaleDateString()}`)

export const colors = writable(['#eaebe7','#111a2a'])